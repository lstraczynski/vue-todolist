# vue-todolist

## Overview
This is small todo list application built in Vue.js framework. App automatically saves list items into local storage.

### List of `todo-list` component props:

#### items
Type: `Array`<br>
Required: `true`<br>

List of objects (todo items) in the following format:
```
{
	itemId: (number|string) // unique item ID
	content: (string) // todo item content
	completed: (boolean) // is todo item marked as completed
}
```
Used as a prop for a `v-model` directive.

#### noItemsMessage
Type: `String`<br>
Required: `false`<br>
Default: `There are no items on the list :-(`<br>

Message that will be displayed when there is no item on the list.

#### autofocusOnLastItem
Type: `Boolean`<br>
Required: `false`<br>
Default: `true`<br>

Should the last item be autofocused when adding new element to the list

#### moveCompletedToBottom
Type: `Boolean`<br>
Required: `false`<br>
Default: `false`<br>

Should completed items be moved to the bottom

### List of `todo-list` component events:

#### change

Parameters:
* `list` - list of todo items
* `item` - changed item
* `action` - action that has been performed on the item. Possible values are: `added`, `modified` and `removed`.

Called whenever any item has been modified, added or removed.

#### item:added

Parameters:
* `item` - added item

Called whenever item has been added to the list.

#### item:modified

Parameters:
* `item` - modified item

Called whenever item in the list has been modified.

#### item:removed

Parameters:
* `item` - removed item

Called whenever item has been removed from the list.

## Installation

### Project setup
```
npm install
```

### Run application
```
npm run serve
```

### Compilation and minification for production
```
npm run build
```

