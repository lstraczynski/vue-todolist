import Vue from 'vue';
import App from '@/App.vue';
import '@/assets/global.scss';
import '@mdi/font/scss/materialdesignicons.scss';

Vue.config.productionTip = false;

new Vue({
	render: h => h(App),
}).$mount('#app');
