function debounce(func, wait) {
	let timeoutId = null;

	return function (...args) {
		if (timeoutId === null || timeoutId) {
			clearTimeout(timeoutId);
			timeoutId = setTimeout(() => {
				timeoutId = null;
				func(...args);
			}, wait);
		}
	}
}

export {debounce};